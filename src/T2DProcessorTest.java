package com.jelurida.ardor.contracts;

import nxt.addons.JO;
import nxt.http.callers.IssueAssetCall;
import nxt.http.callers.SendMessageCall;
import nxt.http.callers.SetAccountPropertyCall;
import nxt.util.Convert;
import nxt.util.Logger;
import org.junit.Assert;
import org.junit.Test;

import static nxt.blockchain.ChildChain.IGNIS;

public class T2DProcessorTest extends AbstractContractTest {

    @Test
    public void simple() {
        // Alice and Bob are users, Chuck is the group account and Dave is the moderator
        JO response = IssueAssetCall.create(2).name("T2D").quantityQNT(1000000000).decimals("2").description("").secretPhrase(ALICE.getSecretPhrase()).feeNQT(10000000000L).call();
        long assetId = Convert.fullHashToId(response.parseHexString("fullHash"));
        Logger.logInfoMessage(response.toJSONString());
        generateBlock();

        JO setupParams = new JO();
        setupParams.put("assetId", assetId);
        setupParams.put("groupModeratorAccount", DAVE.getRsAccount());
        setupParams.put("groupAccount", CHUCK.getRsAccount());
        String contractName = ContractTestHelper.deployContract(T2DProcessor.class, setupParams);
        Logger.logInfoMessage(contractName);


        response = SetAccountPropertyCall.create(2).property("group").value("G1").recipient(ALICE.getRsAccount()).feeNQT(IGNIS.ONE_COIN).secretPhrase(CHUCK.getSecretPhrase()).call();
        Logger.logInfoMessage(response.toJSONString());
        response = SetAccountPropertyCall.create(2).property("group").value("G1").recipient(BOB.getRsAccount()).feeNQT(IGNIS.ONE_COIN).secretPhrase(CHUCK.getSecretPhrase()).call();
        Logger.logInfoMessage(response.toJSONString());

        JO message = new JO();
        message.put("wt", "91");
        message.put("sl", "105");
        message.put("group", "G1");
        response = SendMessageCall.create(2).message(message.toJSONString()).recipient(CHUCK.getRsAccount()).feeNQT(IGNIS.ONE_COIN).secretPhrase(DAVE.getSecretPhrase()).call();
        Logger.logInfoMessage(response.toJSONString());
        generateBlock();

        message = new JO();
        message.put("wtd", "-1.5");
        message.put("sld", "-1");
        response = SendMessageCall.create(2).messageIsPrunable(true).message(message.toJSONString()).recipient(CHUCK.getRsAccount()).feeNQT(IGNIS.ONE_COIN).secretPhrase(ALICE.getSecretPhrase()).call();
        Logger.logInfoMessage(response.toJSONString());

        message = new JO();
        message.put("wtd", "0.5");
        message.put("sld", "-2.5");
        response = SendMessageCall.create(2).messageIsPrunable(true).message(message.toJSONString()).recipient(CHUCK.getRsAccount()).feeNQT(IGNIS.ONE_COIN).secretPhrase(BOB.getSecretPhrase()).call();
        Logger.logInfoMessage(response.toJSONString());
        generateBlock(); // contract runs here
        generateBlock();
        Assert.assertEquals(1000000000 - 625, ALICE.getAssetQuantityDiff(assetId));
        Assert.assertEquals(625, BOB.getAssetQuantityDiff(assetId));
    }

}
