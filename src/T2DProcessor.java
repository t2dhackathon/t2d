package com.jelurida.ardor.contracts;

import nxt.addons.AbstractContract;
import nxt.addons.BlockContext;
import nxt.addons.ContractParametersProvider;
import nxt.addons.ContractSetupParameter;
import nxt.addons.JO;
import nxt.http.callers.GetAccountPropertiesCall;
import nxt.http.callers.GetBlockCall;
import nxt.http.callers.GetBlockchainTransactionsCall;
import nxt.http.callers.GetPrunableMessagesCall;
import nxt.http.callers.ReadMessageCall;
import nxt.http.callers.TransferAssetCall;
import nxt.http.responses.TransactionResponse;
import nxt.util.Logger;

import java.util.List;
import java.util.stream.Collectors;

public class T2DProcessor extends AbstractContract {

    @ContractParametersProvider
    public interface Params {

        @ContractSetupParameter
        default int processingInterval() {
            return 5;
        }

        @ContractSetupParameter
        default int amountPerKg() {
            return 100;
        }

        @ContractSetupParameter
        default int amountPerSugarLevel() {
            return 150;
        }

        @ContractSetupParameter
        default String groupModeratorAccount() {
            return "ARDOR-UUWZ-WUYW-AKR6-3YKQF";
        }

        @ContractSetupParameter
        default String groupAccount() {
            return "ARDOR-LRV9-JA2G-GFTL-5SLB9";
        }

        @ContractSetupParameter
        default long assetId() {
            return 5689568692775141614L;
        }
    }

    /**
     * Process new block
     * @param context the block context
     */
    @Override
    public JO processBlock(BlockContext context) {
        Params params = context.getParams(Params.class);
        if (context.getHeight() % params.processingInterval() != 0) {
            return context.generateInfoResponse("N/A");
        }
        JO blockResponse = GetBlockCall.create().height(context.getHeight() - params.processingInterval()).call();
        int blockTimestamp = blockResponse.getInt("timestamp");

        // Load last message from moderator
        JO response = GetBlockchainTransactionsCall.create(2).account(params.groupModeratorAccount()).type(1).subtype(0).
                firstIndex(0).lastIndex(1).call();
        List<JO> transactions = response.getJoList("transactions");
        JO transaction = transactions.get(0);
        JO messageResponse = ReadMessageCall.create(2).transactionFullHash(transaction.getString("fullHash")).call();
        String messageStr = messageResponse.getString("message");
        JO messageJson = JO.parse(messageStr);
        float baselineWeight = messageJson.getFloat("wt");
        float baselineSugarLevel = messageJson.getFloat("sl");
        String group = messageJson.getString("group");

        // Find accounts which belongs to this group
        JO propertiesResponse = GetAccountPropertiesCall.create().setter(params.groupAccount()).property("group").call();
        List<JO> properties = propertiesResponse.getJoList("properties");
        List<String> userAccounts = properties.stream().filter(p -> p.getString("value").equals(group)).map(p -> p.getString("recipient")).
                collect(Collectors.toList());
        float currentWeight = baselineWeight;
        float currentSugarLevel = baselineSugarLevel;
        for (String account : userAccounts) {
            response = GetPrunableMessagesCall.create(2).account(account).otherAccount(params.groupAccount()).timestamp(blockTimestamp).call();
            List<JO> prunableMessages = response.getJoList("prunableMessages");
            for (JO messageObj : prunableMessages) {
                messageStr = messageObj.getString("message");
                if (messageStr == null) {
                    continue;
                }
                try {
                    messageJson = JO.parse(messageStr);
                } catch (Exception e) {
                    Logger.logInfoMessage(e.toString());
                    continue;
                }
                float weightDelta = messageJson.getFloat("wtd");
                float sugarLevelDelta = messageJson.getFloat("sld");
                currentWeight += weightDelta;
                currentSugarLevel += sugarLevelDelta;
            }
        }
        float weightDelta = currentWeight - baselineWeight;
        float sugarLevelDelta = currentSugarLevel - baselineSugarLevel;

        long rewardQuantity = 0;
        if (weightDelta < 0) {
            rewardQuantity -= ((long)(100 * weightDelta) * params.amountPerKg()) / 100;
        }
        if (sugarLevelDelta < 0 ) {
            rewardQuantity -= ((long)(100 * sugarLevelDelta) * params.amountPerSugarLevel()) / 100;
        }
        if (rewardQuantity == 0) {
            return context.generateInfoResponse("Calculated reward us 0");
        }
        for (String account : userAccounts) {
            TransferAssetCall transferAsset = TransferAssetCall.create(2).asset(params.assetId()).recipient(account).quantityQNT(rewardQuantity);
            context.createTransaction(transferAsset);
        }
        return context.getResponse();
    }

    @Override
    public boolean isDuplicate(TransactionResponse myTransaction, List existingUnconfirmedTransactions) {
        return false;
    }
}
